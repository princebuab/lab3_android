package cs.mad.flashcards.activities

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.entities.FlashcardSet


class Adapter(input: List<FlashcardSet>): RecyclerView.Adapter<Adapter.ViewHolder>() {

    private val myData = input.toMutableList()


    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val textView = view.findViewById<TextView>(R.id.my_text)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(View.inflate(parent.context, R.layout.item, null))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textView.text = myData[position].title
    }

    override fun getItemCount(): Int {
        return myData.size
    }
}